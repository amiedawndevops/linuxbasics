#!/bin/sh

#Get User Input
echo "Please enter first number: "
read x

echo "Please enter second number: "
read y

echo "Enter operation (add, sub, mul, div, or mod) and 2 numbers: "
read operation

#Switch Case
case $operation in
	"add")answer=`expr $x + $y` ;;
	"sub")answer=`expr $x - $y` ;;
	"mul")answer=`expr $x \* $y` ;;
	"div")answer=`expr $x / $y` ;;
	"mod")answer=`expr $x % $y` ;;
        *)echo "Please try again with a valid operation: " ;;
esac
echo "Answer: $answer"

